<?php

namespace Drupal\maestro_group_security\Controller;

use Drupal\Core\Form\FormState;
use Drupal\Core\Controller\ControllerBase;
use Drupal\group\Entity\Group;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\maestro_group_security\MaestroGroupSecurityUtilities;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   *
   */
  public static function flagTest() {

    MaestroGroupSecurityUtilities::flagAssignments(1);
    MaestroGroupSecurityUtilities::flagAssignments(0);
    echo "done";
    die();

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('group.membership_loader'),
      $container->get('cache_context.group_membership.roles'),
      $container->get('cache_context.group_membership')
    );
  }

  /**
   *
   */
  public function testNotifications() {

    // Return MaestroGroupSecurityUtilities::getNotificationUserList()
  }

  /**
   *
   */
  public function fetchNotificationRoles(array &$form, FormState $form_state) {

    return $form['edit_task_notifications']['select_notification_group_role'];
  }

  /**
   * Fetchroles.
   */
  public function fetchRoles(array &$form, FormState $form_state) {

    // $assignment = $form_state->getUserInput();
    // $form_state->setValue('selected_group',$assignment['edit_task_assignments']['select_assigned_group']);
    //  $form_state->setRebuild(TRUE);
    /*
    $form['group_assignments']['edit_group_task_assignments']['select_assigned_group_roles']['#title'] = 'TEST TITLE '.rand();

    $form['group_assignments']['edit_group_task_assignments']['select_assigned_group_roles'][6] = $form['group_assignments']['edit_group_task_assignments']['select_assigned_group_roles'][1];
    // kpr(array_keys($form['group_assignments']['edit_group_task_assignments']['select_assigned_group_roles'][1]));
     */

    // $option_template = $form['group_assignments']['edit_group_task_assignments']['select_assigned_group_roles'][1];
    // kpr( $form['group_assignments']);
    // $state = $form_state->getUserInput();
    // $form['edit_task_assignments']['select_assigned_group']=$state['select_assigned_group'];
    // dpr($assignment);
    // if ($assignment['select_assigned_to']=="group") {.
    // $form_state->set('num_names', $add_button);
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    // $form_state->setRebuild();
    // dpr($form_state);
    // dpr($form['edit_task_assignments']['select_assigned_group_roles']['All']);
    /*
    $form['edit_task_assignments']['select_assigned_group_roles']['BO'] = $form['edit_task_assignments']['select_assigned_group_roles']['Roles'];
    $form['edit_task_assignments']['select_assigned_group_roles']['BO']['#title']='Budget Officer';
    $form['edit_task_assignments']['select_assigned_group_roles']['BO']['#return_value']='budget_officer';
    $form['edit_task_assignments']['select_assigned_group_roles']['BO']['#default_value']='budget_officer';

    $form['edit_task_assignments']['select_assigned_group_roles']['COOR'] = $form['edit_task_assignments']['select_assigned_group_roles']['Roles'];
    $form['edit_task_assignments']['select_assigned_group_roles']['COOR']['#title']='Coordinator';
    $form['edit_task_assignments']['select_assigned_group_roles']['COOR']['#return_value']='coordinator';
    $form['edit_task_assignments']['select_assigned_group_roles']['COOR']['#default_value']='coordinator';
     */

    // $response = new AjaxResponse();
    // $response->addCommand(new InvokeCommand(NULL, 'showGroupForm', ['test']));
    // return $response;
    // $form['edit_task_assignments']['select_method']['#validated']= True;
    //  $form['edit_task_assignments']['select_assigned_group_roles']['#validated']= True;
    // $form['autoresult']['#validated'] = True;
    // kpr(($form['group_assignments']));.
    // $form['edit_task_assignments']['select_assigned_group_roles']['#options'] = [1 => 1, 3 => 3];.
    $form['edit_task_assignments']['select_assigned_group_role']['svalue'] = ['stuff' => 'stuff', 'things' => 'things'];

    return $form['edit_task_assignments']['select_assigned_group_role'];

    // }
    //  }else {
    //   return [];
    // }.
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return mixed
   */
  public function fetchRolesInteractive(Request $request) {

    $group = $request->get('q');

    $group = trim(preg_split('/(\([^\)]+\))/', $group)[0]);

    $query = \Drupal::entityQuery('group');
    $query->condition('label', $group, 'CONTAINS');
    $groups = $query->execute();
    foreach ($groups as $group) {
      $results[] = Group::load($group)->label() . ' (' . $group . ')';

    }

    return new JsonResponse($results);

  }

  /**
   *
   */
  public function fetchRolesMembershipIfTask(array &$form, FormState $form_state) {

    return $form['byassignment']['select_assigned_group_roles'];

  }

  /**
   *
   */
  public function fetchRolesIfTask(array &$form, FormState $form_state) {

    return $form['byassignment']['select_assigned_group_roles'];

  }

  /**
   *
   */
  public function buildGroupTypeSelect(array &$form, FormState $form_state) {

    return $form['edit_group_task_assignments']['variable']['select_assigned_variable_roles'];

  }

  /**
   *
   */
  public function buildGroupTypeSelectIf(array &$form, FormState $form_state) {

    return $form['byvariable']['select_assigned_group_by_var_roles'];

  }

  /**
   *
   */
  public function buildGroupTypeSelectInteractive(array &$form, FormState $form_state) {

    return $form['edit_task_assignments']['select_assigned_group_roles'];

  }

}
