<?php

namespace Drupal\maestro_group_security;

use Drupal\Core\Access\AccessResult;
use Drupal\flag\Entity\Flag;






use Drupal\user\Entity\User;
use Drupal\group\Entity\Group;
use Drupal\group\GroupMembershipLoader;
use Drupal\maestro\Entity\MaestroQueue;
use Drupal\maestro\Engine\MaestroEngine;
use Drupal\maestro\Entity\MaestroProcess;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\maestro\Entity\MaestroProductionAssignments;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
 
/**
 * Class MaestroGroupSecurityUtilities.
 */
class MaestroGroupSecurityUtilities implements MaestroGroupSecurityUtilitiesInterface {

  /**
   * Symfony\Component\DependencyInjection\ContainerAwareInterface definition.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerAwareInterface
   */
  protected $entityQuery;
  /**
   * Drupal\group\GroupMembershipLoaderInterface definition.
   *
   * @var \Drupal\group\GroupMembershipLoaderInterface
   */
  protected $groupMembershipLoader;

  /**
   * Constructs a new MaestroGroupSecurityUtilities object.
   */
  public function __construct(ContainerAwareInterface $entity_query, GroupMembershipLoaderInterface $group_membership_loader) {
    $this->entityQuery = $entity_query;
    $this->groupMembershipLoader = $group_membership_loader;
  }

  /**
   * @param $id
   * @param $type
   * @param bool $bundle
   * @return null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getQueueIdFromEntity($type,$id, $bundle = false) {



     $value = NULL;

    $query = \Drupal::entityQuery('maestro_entity_identifiers')
      ->condition('entity_id', $id);
    $query->condition('entity_type',$type);
    if ($bundle) {
      $query->condition('bundle',$bundle);
    }

    $entityID = current($query->execute());

    if($entityID) {

      $record = \Drupal::entityTypeManager()->getStorage('maestro_entity_identifiers')->load($entityID);
      $qq = \Drupal::entityQuery('maestro_queue')
        ->condition('process_id',$record->process_id->getString())
        ->condition('status',0);

      $qs = $qq->execute();
      $s = MaestroQueue::load(end($qs));
      if($s) $value = $s->id();
    }
    return $value;

}
  /**
   *
   */
  public static function tagProcessHistory($pid) {
    $process = MaestroProcess::load($pid);
    $users = self::getCurrentNotificationList($pid);

    foreach ($users as $email) {
      $account = user_load_by_mail($email);
      $flag_service = \Drupal::service('flag');
      $flag = Flag::load('participation');
      if (!$account) {
        $account = NULL;
      }
      if (!$flag->isFlagged($process, $account)) {

        $flag_service->flag($flag, $process, $account);
      }

    }

  }

  /**
   *
   */
  public static function getNotificationUserList($thisNotification, $pid) {

    $notificationList = [];
    if ($thisNotification[0] == 'group') {

      switch ($thisNotification[1]) {

        case "fixed":

          $gstring = explode("(", $thisNotification[2])[1];
          $roles = explode(")", $gstring)[1];
          $roles = ltrim($roles, '|');
          $roles = explode("|", $roles);
          $gstring = explode(")", $gstring)[0];

          $roleList = [];
          foreach ($roles as $key => $role) {
            if ($role != "") {
              $roleList[] = $role;
            }
          }

          $group = Group::load($gstring);

          if ($group) {
            $members = $group->getMembers();

            /** @var \Drupal\group\GroupMembership $member */
            foreach ($members as $member) {
              /** @var \Drupal\user\Entity\User $user */
              $user = $member->getUser();

              if (count($roleList) > 0) {

                $mroles = [];
                /** @var  \Drupal\group\Entity\GroupRole $mr */
                foreach ($member->getRoles() as $mr) {
                  $mroles[] = $mr->label();
                }

                foreach ($roleList as $notiRole) {
                  if (in_array($notiRole, $mroles)) {
                    $notificationList[] = $user->get('name')->value;

                  }
                }
              }
              else {
                $notificationList[] = $user->get('name')->value;

              }

            }

          }

          break;

        case "variable":

          $roles = $thisNotification[2];

          $roles = ltrim($roles, '|');
          $roles = explode("|", $roles);
          $varname = $roles[0];
          $roleList = [];

          foreach ($roles as $key => $role) {

            $roleList[] = $role;
          }

          $group = Group::load(MaestroEngine::getProcessVariable($varname, $pid));

          if ($group) {
            $members = $group->getMembers();

            /** @var \Drupal\group\GroupMembership $member */
            foreach ($members as $member) {
              /** @var \Drupal\user\Entity\User $user */
              $user = $member->getUser();
              /** @var \Drupal\group\GroupRole $userRole */

              foreach ($member->getRoles() as $userRole) {

                if (count($roleList) == 0 || in_array($userRole->label(), $roleList)) {
                  $notificationList[] = $user->get('name')->value;
                  break;
                }
              }

            }

          }

          break;
      }

    }

    return $notificationList;

  }

  /**
   *
   */
  public static function getTaskDetails($procid, $taskid) {

    $templateMachineName = MaestroEngine::getTemplateIdFromProcessId($procid);
    $template = MaestroEngine::getTemplate($templateMachineName);
    $var_workflow_stage_count = intval(MaestroEngine::getProcessVariable('workflow_timeline_stage_count', $procid));

    // workflow_current_stage.
    $currstage = intval(MaestroEngine::getProcessVariable('workflow_current_stage', $procid));
    $currstagemessage = MaestroEngine::getProcessVariable('workflow_current_stage_message', $procid);
    $stage_history = MaestroEngine::getAllStatusEntriesForProcess($procid);
    $stage_history[$currstage]['message'] = $currstagemessage;

    /*

    foreach ($stage_history as $stage) {
    if ($stage['completed'] == '') {
    $currstage = $stage['stage_number'];
    $currstagemessage = $stage['message'];
    break;

    }
    }
    echo "<pre>";
    print_r($currstage);
    die();

     */

    $submission = MaestroEngine::getAllEntityIdentifiersForProcess($procid);


    if (isset($submission['submission'])) {
      $submission = $submission['submission'];
      $submissionEntity = WebformSubmission::load($submission['entity_id']);
      if ($submission['entity_type'] && $submissionEntity) {
        $view_builder = \Drupal::entityTypeManager()->getViewBuilder($submission['entity_type']);

        $pre_render = $view_builder->view($submissionEntity, 'default');

        /** @var \Drupal\Core\Render\Markup $submissionView */
        $submissionView = render($pre_render);
      }
      else {
        $submissionView = '';
      }

    }
    else {
      $submission = FALSE;
      $submissionView = FALSE;

    }

    $tasks = [];

    foreach ($template->tasks as $task) {

      if (isset($task['participate_in_workflow_status_stage']) && $task['participate_in_workflow_status_stage'] == TRUE) {
        $tasks[$task['workflow_status_stage_number']] = [
         // 'label' => $task['label'],.
          'label' => $task['label'],
          'id' => $task['id'],
          'message' => $task['workflow_status_stage_message'],
          'complete' => $task['workflow_status_stage_number'] < $currstage ? TRUE : FALSE,
          'current' => $task['workflow_status_stage_number'] == $currstage ? TRUE : FALSE,
          'final' => $task['workflow_status_stage_number'] == $var_workflow_stage_count ? TRUE : FALSE,
        ];

        if ($task['workflow_status_stage_number'] == $currstage) {

          $tasks[$task['workflow_status_stage_number']]['message_override'] = $stage_history[$task['workflow_status_stage_number']]['message'];
        }
      }
    }

    $qs = \Drupal::entityQuery('maestro_queue');
    $qs->condition('process_id', $procid);
    $qs->condition('completed', '', '<>');

    $queues = MaestroQueue::loadMultiple($qs->execute());

    $history = [];
    $lastActiveUser = User::load(0)->toArray();
    foreach ($queues as $q) {

      $qa = $q->toArray();

      $user = User::load($qa['uid'][0]['value'])->toArray();

      if ($qa['uid'][0]['value'] != 0) {
        $lastActiveUser = $user;
      }
      $history[$qa['id'][0]['value']] = $qa;
      $history[$qa['id'][0]['value']]['user'] = $lastActiveUser;

    }

    $ret = [];
    ksort($tasks);
    $ret['process_id'] = $procid;
    $ret['task_id'] = $taskid;
    $ret['unique_id'] = $procid . $taskid;
    $ret['current_stage'] = $currstage;
    $ret['stages'] = $tasks;
    $ret['stage_history'] = $stage_history;
    $ret['submission'] = [];
    $ret['submission']['info'] = $submission;
    $ret['submission']['view'] = $submissionView;
    $s2 = MaestroEngine::getAllEntityIdentifiersForProcess($procid);

    foreach ($s2 as $sub) {



      $websub = \Drupal::entityManager()->getStorage($sub['entity_type'])->load($sub['entity_id']);
      $label = '';
      if ($websub && $websub->getElementData('sc_attachment_name')) {
        $label = $websub->getElementData('sc_attachment_name');
      }
      else {
        if ($websub) {
          $label = $websub->label();
          $label = explode(":", $label)[0];
          $label = trim($label);

        }
        else {
          $label = "Missing Submission";

        }
        /*
        $label = $websub->label();
        $label = explode(":", $label)[0];
        $label = trim($label);
         */

      }

      $tmp2 = [];
      $tmp2['info']['request_id'] = isset($sub['entity_id']) ? $sub['entity_id'] : FALSE;
      $tmp2['info']['request_type'] = isset($sub['entity_type']) ? $sub['entity_type'] : FALSE;
      $tmp2['info']['request_unique_id'] = isset($sub['unique_id']) ? $sub['unique_id'] : FALSE;
      $tmp2['info']['request_bundle'] = isset($sub['bundle']) ? $sub['bundle'] : FALSE;
      $tmp2['info']['request_label'] = $label;

      $submissionEntity = WebformSubmission::load($sub['entity_id']);
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder($sub['entity_type']);
      if ($submissionEntity) {
        $pre_render = $view_builder->view($submissionEntity, 'default');
        /** @var \Drupal\Core\Render\Markup $submissionView */
        $submissionView = render($pre_render);
      }
      else {
        $submissionView = '';
      }



      $tmp2['view'] = $submissionView;
      $ret['attachments'][] = $tmp2;

    }

    $ret['history'] = $history;

    return $ret;
  }



  /**
   *
   */
  public static function parseVariableUserString(String $assignment) {
    $roles = FALSE;
    $assignment = explode(":", $assignment);
    $group = $assignment[2];

    // print_r( explode("(", $assignment[0]));.
    $group = explode("(", $group)[0];
    $gid = explode("(", $assignment[2])[1];
    $roles = explode(")", $gid)[1];
    $gid = explode(")", $gid)[0];
    $roles = explode("|", $roles);

    return [
      'gid' => $gid,
      'group' => trim($group),
      'roles' => $roles,
    ];

  }


  /**
   *
   */
  public static function parseFixedGroupString(String $assignment) {
    $roles = FALSE;
    $assignment = explode(":", $assignment);
    $group = $assignment[2];

    // print_r( explode("(", $assignment[0]));.
    $group = explode("(", $group)[0];
    $gid = explode("(", $assignment[2])[1];
    $roles = explode(")", $gid)[1];
    $gid = explode(")", $gid)[0];
    $roles = explode("|", $roles);

    return [
      'gid' => $gid,
      'group' => trim($group),
      'roles' => $roles,
    ];

  }

  /**
   *
   */
  public static function getUserGroups($userID = FALSE) {
    if (!$userID) {
      $uid = \Drupal::currentUser()->id();
    }
    /** @var Drupal\group\GroupMembershipLoader $gml */
    $gml = \Drupal::service('group.membership_loader');
    $userGroups = [];

    $acc = User::load($userID);

    /** @var Drupal\group\GroupMembership $group */
    foreach ($gml->loadByUser($acc) as $group) {

      $userGroups[$group->getGroup()->label()]['label'] = $group->getGroup()
        ->label();
      $code = false;
      if ($group->getGroup()->field_code) {
        $code = $group->getGroup()->field_code->value;
      }
      $userGroups[$group->getGroup()->label()]['code'] = $code;


      $userGroups[$group->getGroup()->label()]['id'] = $group->getGroup()->id();
      $roles = $group->getRoles();
      $options = FALSE;

      foreach ($roles as $key => $role) {
        $options[trim($role->label())] = trim($role->label());
      }

      $userGroups[$group->getGroup()->label()]['roles'] = $options;

    }

    return $userGroups;

  }

  /**
   *
   */
  public static function checkAccessFixed($ginfo, $userGroups) {
    $access = FALSE;
    $targettitle = '';
    $ginfo = explode(":", $ginfo);
    $roles = [];
    if (isset($ginfo[2]) && strpos($ginfo[2], '|')) {

      $rolesplit = explode("|", $ginfo[2]);

      $targetTitle = $rolesplit[0];
      unset($rolesplit[0]);

      foreach ($rolesplit as $role) {
        $roles[] = $role;
      }

    }
    else {
      if (isset($ginfo[2])) {
        $targetTitle = $ginfo[2];
      }
    }

    $targetTitle = explode("(", $targetTitle);
    $title = $targetTitle[0];
    if (isset($targetTitle[1])) {
      $id = rtrim($targetTitle[1], ')');
    }
    else {
      $id = -1;
    }

    $title = rtrim($title);

    if (array_key_exists($title, $userGroups)) {
      if (count($roles) > 0) {
        foreach ($roles as $role) {
          if (in_array($role, array_keys($userGroups[$title]['roles']))) {
            $access = TRUE;
          }
        }
      }
      else {
        $access = TRUE;
      }
    }

    return $access;

  }

  /**
   *
   */
  public static function checkAccessVariable($ginfo, $userGroups, $pid) {

    $access = FALSE;
    $ginfo = explode(":", $ginfo);
    $roles = [];
    if (isset($ginfo[2]) && strpos($ginfo[2], '|')) {

      $rolesplit = explode("|", $ginfo[2]);

      $targetTitle = $rolesplit[0];
      unset($rolesplit[0]);

      foreach ($rolesplit as $role) {
        $roles[] = $role;
      }

    }
    else {
      if (isset($ginfo[2])) {
        $targetTitle = $ginfo[2];
      }
    }

    $varname = $targetTitle;
    $groupLookup = Group::load(MaestroEngine::getProcessVariable($varname, $pid));

    if (!$groupLookup) {
      return FALSE;
    }

    if (array_key_exists(trim($groupLookup->label()), $userGroups)) {

      if (count($roles) > 0) {

        foreach ($roles as $role) {

          if (in_array($role, array_keys($userGroups[$groupLookup->label()]['roles']))) {

            $access = TRUE;
          }
        }
      }
      else {
        $access = TRUE;
      }
    }

    return $access;

  }

  /**
   *
   */
  public static function flagAssignments($assignmentIDs = FALSE, $complete = 0) {




    \Drupal::entityManager()->getViewBuilder('maestro_production_assignments')->resetCache();
    $flagService = \Drupal::service('flag');
    $flag = Flag::load('group_access');

    if ($assignmentIDs) {
      if (!is_array($assignmentIDs)) {
        return FALSE;
      }
    }
    else {
      $query = \Drupal::entityQuery('maestro_production_assignments');
      $and = $query->andConditionGroup();
      $and->condition('assign_type', 'group');
      $and->condition('task_completed', $complete);
      $query->condition($and);
      $assignmentIDs = $query->execute();
    }

    $assignments = MaestroProductionAssignments::loadMultiple($assignmentIDs);
    // Remove flags for *open* assignments
    // $flagService->flag($flag,array_shift($assignments));
    $query = \Drupal::entityTypeManager()->getStorage('flagging')->getQuery();
    $query->condition('flag_id', 'group_access');
    $query->condition('entity_id', $assignmentIDs, "IN");
    $ids = $query->execute();
    $flaggings = \Drupal::entityTypeManager()->getStorage('flagging')->loadMultiple($ids);
    \Drupal::entityTypeManager()->getStorage('flagging')->delete($flaggings);

    foreach ($assignments as $assign) {
      // Get list of users with group access to this assignment.
      /** @var Drupal\group\GroupMembershipLoader $gml */
      $gml = \Drupal::service('group.membership_loader');
      if ($assign->assign_type->value != "group") {

        // continue;.
      }

      if (!strpos($assign->assign_id->value, ':')) {

        continue;
      }

      $parsed = '';
      switch ($assign->by_variable->value) {
        case "1":

          $queueRecord = MaestroEngine::getQueueEntryById($assign->queue_id->getString());
          $pid = $queueRecord->process_id->getString();
          $parsed = self::parseVariableGroupString($assign->assign_id->value, $pid);



          break;

        case "0":

          $parsed = self::parseFixedGroupString($assign->assign_id->value);

          break;
      }

      if ($parsed['gid'] == '') {
        continue;
      }

      $group = Group::load($parsed['gid']);

      $filters = NULL;
      $groupTranslate = [];
      /** @var \Drupal\group\Entity\GroupType $gtype */
      $gtype = $group->getGroupType();
      /** @var \Drupal\group\Entity\GroupRole $role */
      foreach ($gtype->getRoles() as $role) {
        $tmp = [];
        $tmp['key']=$role->id();
        $tmp['label']=$role->label();
        $groupTranslate[$role->label()] = $role->id();
      }


      foreach ($parsed['roles'] as $role) {
        if ($role != "") {
          $filters[] = $groupTranslate[$role];
        }
      }

      $membership = $gml->loadByGroup($group, $filters);

      foreach ($membership as $member) {
       $user = $member->getUser();
        if (!$flag->isFlagged($assign, $user)) {
          $flagService->flag($flag, $assign, $user);
        }
      }






    }



  }

  /**
   *
   */
  public static function parseVariableGroupString($ginfo, $pid) {

    $ginfo = explode(":", $ginfo);
    $roles = [];
    if (isset($ginfo[2]) && strpos($ginfo[2], '|')) {

      $rolesplit = explode("|", $ginfo[2]);

      $targetTitle = $rolesplit[0];
      unset($rolesplit[0]);

      foreach ($rolesplit as $role) {
        $roles[] = $role;
      }

    }
    else {
      if (isset($ginfo[2])) {
        $targetTitle = $ginfo[2];
      }
    }

    $varname = $targetTitle;
    $groupLookup = MaestroEngine::getProcessVariable($varname, $pid);
    $gg = Group::load($groupLookup);
    if ($gg) {

      return [
        'gid' => $groupLookup,
        //'group' => 'label missing - loaded by variable',
        'group' => trim($gg->label()),
        'roles' => $roles,
      ];

    } else {
      return [
        'gid' => false,
        //'group' => 'label missing - loaded by variable',
        'group' => false,
        'roles' => [],
      ];
    }
  }

  /**
   *
   */
  public static function getGroupAssignmentsByFlag($completed = 0, $userID = FALSE, $limit = 0) {
    switch ($completed) {
      case 0:
        $cflag = 0;
        $aflag = 0;
        $sflag = 0;
        break;

      case 1:
        $cflag = 1;
        $aflag = 1;
        $sflag = 1;
        break;
    }

    $queueIDs = [];
    $prods = [];
    if (!$userID) {
      $userID = \Drupal::currentUser()->id();
    }
    $query = \Drupal::entityTypeManager()->getStorage('flagging')->getQuery();
    $query->condition('flag_id', 'group_access');
    $query->condition('uid', $userID);
    $flags = \Drupal::entityTypeManager()->getStorage('flagging')->loadMultiple($query->execute());
    foreach ($flags as $flag) {
      $prods[] = $flag->entity_id->value;
    }
    $assignments = MaestroProductionAssignments::loadMultiple($prods);

    $pids = [];
    foreach ($assignments as $assign) {
      if ($assign->task_completed->value == $completed) {

        $q = MaestroQueue::load($assign->queue_id->target_id);

        $queueRecord = MaestroEngine::getQueueEntryById($assign->queue_id->getString());
        $processRecord = MaestroEngine::getProcessEntryById($queueRecord->process_id->getString());

        if ($queueRecord && $processRecord) {

          if (
            $queueRecord->status->getString() == $sflag
            && $queueRecord->archived->getString() == $aflag
            && $processRecord->complete->getString() == $cflag
          ) {

            if (!in_array($queueRecord->process_id->getString(), $pids)) {
              $pids[] = $queueRecord->process_id->getString();
              $queueIDs[] = $assign->queue_id->getString();

            }

          }

        }
      }
    }

    return array_unique($queueIDs);

  }

  /**
   *
   */
  public static function getGroupAssignments($completed = 0, $userID = FALSE, $limit = 0) {



    switch ($completed) {
      case 0:
        $cflag = 0;
        $aflag = 0;
        $sflag = 0;
        break;

      case 1:
        $cflag = 1;
        $aflag = 1;
        $sflag = 1;
        break;
    }

    $queueIDs = [];
    if (!$userID) {
      $userID = \Drupal::currentUser()->id();
    }

    $userGroups = self::getUserGroups($userID);

    $query = \Drupal::entityQuery('maestro_production_assignments');
    $or = $query->orConditionGroup();
    $and = $query->andConditionGroup();
    $cond = $query->andConditionGroup();
    foreach ($userGroups as $key => $group2) {
      $or->condition('assign_id', $group2['label'], 'CONTAINS');
    }
    $or->condition('by_variable', 1);
    $and->condition('assign_type', 'group');
    $and->condition('task_completed', $completed);

    $cond->condition($or);
    $cond->condition($and);
    $query->condition($cond);
    // $query->condition($or);
    // $query->condition($and);
    $assignmentIDs = $query->execute();

    $pids = [];
    $start = time();

    $records = MaestroProductionAssignments::loadMultiple($assignmentIDs);
    foreach ($records as $assignmentRecord) {
      /** @var Drupal\maestro\Entity\MaestroProductionAssignments $assignmentRecord */
      // $assignmentRecord = MaestroProductionAssignments::load($entity_id);
      // Check if this queueID and the associated process is complete or not.
      /** @var Drupal\maestro\Entity\MaestroQueue $queueRecord */
      $queueRecord = MaestroEngine::getQueueEntryById($assignmentRecord->queue_id->getString());
      $processRecord = MaestroEngine::getProcessEntryById($queueRecord->process_id->getString());
      $task = MaestroEngine::getTemplateTaskByQueueID($assignmentRecord->queue_id->getString());

      if (isset($task['assigned'])) {

        $pretitle = '';
        $asslist = ltrim($task['assigned'], ',');
        $asslist = rtrim($task['assigned'], '|');

        $assignments = explode(",", $asslist);

        foreach ($assignments as $key => $group) {

          $roles = [];
          $title = '';
          $id = '';
          $ginfo = explode(":", $group);
          $method = isset($ginfo[1]) ? $ginfo[1] : '';

          $access = FALSE;

          switch ($method) {
            case "fixed":
              $access = self::checkAccessFixed($group, $userGroups);

              break;

            case "variable":

              $access = self::checkAccessVariable($group, $userGroups, $queueRecord->process_id->getString());

          }

          if ($access == FALSE) {

            continue;
          }

          /** @var \Drupal\group\Entity\GroupInterface $g */
          if ($access && $queueRecord && $processRecord) {

            if (
             $queueRecord->status->getString() == $sflag
                && $queueRecord->archived->getString() == $aflag
            && $processRecord->complete->getString() == $cflag
              ) {

              if (!in_array($queueRecord->process_id->getString(), $pids)) {
                $pids[] = $queueRecord->process_id->getString();
                $queueIDs[] = $assignmentRecord->queue_id->getString();

              }

            }

          }

        }

        /*   ############ */

      }
    }

    return $queueIDs;
  }

  /**
   *
   */
  public static function getCurrentNotificationList($pid) {

    $q = \Drupal::entityquery('maestro_queue');
    $q->condition('process_id', $pid);
    $qs = $q->execute();

    $groups = [];
    foreach ($qs as $qid) {
      $tmp = MaestroEngine::getAssignedNamesOfQueueItem($qid);
      if (count($tmp) > 0) {
        foreach ($tmp as $tg) {
          $tgx = explode(":", $tg);
          $notis = self::getNotificationUserList($tgx, $pid);
          foreach ($notis as $email) {
            $groups[] = $email;
          }

        }

      }

    }

    return $groups;

  }

  public static  function getAssignmentsByProcessID($pid) {

  }

  /**
   *
   */
  public static function checkAssignment(MaestroProductionAssignments $ass) {



    $access = AccessResult::neutral();


    $qid = $ass->toArray()['queue_id'][0]['target_id'];
    $pid = MaestroEngine::getProcessIdFromQueueId($qid);


    switch($ass->assign_type->value) {

      case "user":
        if (isset($ass->by_variable)) {
          switch ($ass->by_variable->value) {
            case "0":
              $acc = $ass->assign_id->value;
              break;

            case "1":
              die("Assigning a user by variable is not yet implemented.");

            //  $acc = self::parseVariabledUserString($ass->assign_id->value,$pid);
              break;
          }


            if ($acc == \Drupal::currentUser()->getAccountName()) {

              $access = AccessResult::allowed();
            }


        }


        break;
      case "group":

          /** */
          /** @var GroupMembershipLoader $gml */
          $gml = \Drupal::service('group.membership_loader');
          //$ugroups = $gml->groupMembershipLoader->loadByUser();
          $ugroups = $gml->loadByUser();
          $ugr = [];
          foreach ($ugroups as $ugroup) {
            $label = $ugroup->getGroup()->id();

            $ugr[$label] = [];
            foreach ($ugroup->getRoles() as $urole) {
              $ugr[$label][] = $urole->label();
            }
          }

          if (isset($ass->by_variable)) {
            switch ($ass->by_variable->value) {
              case "0":
                  $acc = self::parseFixedGroupString($ass->assign_id->value,$pid);
                break;

              case "1":
                  $acc = self::parseVariableGroupString($ass->assign_id->value,$pid);
              break;
            }


            if (isset($ugr[$acc['gid']])) {

              if (count(array_intersect($ugr[$acc['gid']],$acc['roles'])) > 0) {
                $access = AccessResult::allowed();
               // $access = AccessResult::neutral();
              }

            }

          }

        break;

    }


    return $access;

  }

}
