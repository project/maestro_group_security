<?php

namespace Drupal\maestro_group_security\Plugin\EngineTasks;

use Drupal\Core\Plugin\PluginBase;
use Drupal\maestro\MaestroEngineTaskInterface;
use Drupal\maestro\Engine\MaestroEngine;
use Drupal\maestro\MaestroTaskTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\maestro\Form\MaestroExecuteInteractive;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupType;
use Drupal\Core\Session\UserSession;

/**
 * @Plugin(
 *   id = "GroupMembership",
 *   task_description = @Translation("Yes / No task for checking if the submitter is a member of a specific group or group role."),
 * )
 */
class GroupMembershipTask extends PluginBase implements MaestroEngineTaskInterface {

  use MaestroTaskTrait;

  /**
   *
   */
  public function __construct($configuration = NULL) {
    if (is_array($configuration)) {
      $this->processID = $configuration[0];
      $this->queueID = $configuration[1];
    }
  }

  /**
   * {@inheritDoc}.
   */
  public function isInteractive() {
    return FALSE;
  }

  /**
   * {@inheritDoc}.
   */
  public function shortDescription() {
    return t('Group Membership If Task');
  }

  /**
   * {@inheritDoc}.
   */
  public function description() {
    return $this->t('Performs logic on task completion or variables.');
  }

  /**
   * {@inheritDoc}.
   *
   * @see \Drupal\Component\Plugin\PluginBase::getPluginId()
   */
  public function getPluginId() {
    return 'GroupMembership';
  }

  /**
   * {@inheritDoc}.
   */
  public function getTaskColours() {
    return 'purple';
  }

  /**
   * Part of the ExecutableInterface
   * Execution of the Batch Function task will use the handler for this task as the executable function.
   * {@inheritdoc}.
   */
  public function execute() {
    $templateMachineName = MaestroEngine::getTemplateIdFromProcessId($this->processID);
    $taskMachineName = MaestroEngine::getTaskIdFromQueueId($this->queueID);
    $task = MaestroEngine::getTemplateTaskByID($templateMachineName, $taskMachineName);
    $statusFlag = NULL;
    $ifData = $task['data']['GroupIf'];

    switch ($ifData['method']) {

      case "byvariable":
        $ifGroup = MaestroEngine::getProcessVariable($task['data']['GroupIf']['varname'], $this->processID);
        $ifRoles = [];
        foreach ($ifData['vroles'] as $key => $role) {

          if ($role != '') {
            $ifRoles[$key] = $role;
          }
        }

        break;

      case "fixed":

        $ifGroup = $ifData['groups'];
        $ifRoles = $ifData['roles'];
        foreach ($ifRoles as $role => $val) {
          if ($val === 0) {
            unset($ifRoles[$role]);
          }
        }

        break;

    }

    $initiator = MaestroEngine::getProcessVariable('initiator', $this->processID);

    $user = user_load_by_name($initiator);
    $account = new UserSession([
      'uid' => $user->id(),
    ]);

    /** @var \Drupal\group\GroupMembershipLoader $gml */
    $gml = \Drupal::service('group.membership_loader');

    $groups = [];

    foreach ($gml->loadByUser($account) as $group) {

      $groups[$group->getGroup()->label()]['label'] = $group->getGroup()->label();
      $groups[$group->getGroup()->label()]['id'] = $group->getGroup()->id();

      $roles = $group->getRoles();

      $options = FALSE;
      foreach ($roles as $key => $role) {

        $options[$role->label()] = $role->label();
      }

      $groups[$group->getGroup()->label()]['roles'] = $options;

    }

    $statusFlag = FALSE;

    foreach ($groups as $group) {

      if ($group['id'] == $ifGroup) {
        $newRoles = [];
        foreach ($ifRoles as $key => $val) {
          if ($val != '') {
            $newRoles[$key] = $val;
          }

        }

        if (count($newRoles) > 0) {
          foreach ($newRoles as $role) {

            if (in_array($role, $group['roles'])) {

              $statusFlag = TRUE;

            }

          }

        }
        else {
          $statusFlag = TRUE;
        }

      }
    }

    // At this point, we have a statusFlag variable that denotes whether the pointed-from tasks in the queue have
    // a status that is equal to the status that was provided in the template.  If it's false, then we complete this
    // IF task with the execution status as success with a completion status of use the false branch.
    if ($statusFlag !== NULL) {
      if ($statusFlag == TRUE) {
        // Normal condition here. we've not aborted or done anything different.
        $this->executionStatus = TASK_STATUS_SUCCESS;
        // This will follow the true branch.
        $this->completionStatus = MAESTRO_TASK_COMPLETION_NORMAL;
      }
      else {
        // again, nothing unusual.  just set the task status.
        $this->executionStatus = TASK_STATUS_SUCCESS;
        // ahh.. last status doesn't equal what we tested for.  Use the false branch for nextstep.
        $this->completionStatus = MAESTRO_TASK_COMPLETION_USE_FALSE_BRANCH;
      }
      // Nothing really stopping us from always completing this task in the engine.
      return TRUE;
    }

    \Drupal::logger('maestro')
      ->error('If task does not have a statusFlag set and is unable to complete');
    // Problem here - we have a situation where the IF statement is stuck because the statusFlag was never set.
    return FALSE;
    // This will hold the engine at the IF task forever.
  }

  /**
   * {@inheritDoc}.
   */
  public function getExecutableForm($modal, MaestroExecuteInteractive $parent) {

  }

  /**
   * {@inheritDoc}.
   */
  public function handleExecuteSubmit(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritDoc}.
   */
  public function getTaskEditForm(array $task, $templateMachineName) {
    // $ifParms = isset($task['data']['if']) ? $task['data']['if'] : [];.
    $ifParms = isset($task['data']['GroupIf']) ? $task['data']['GroupIf'] : [];

    $gtypes = \Drupal::entityManager()->getBundleInfo('group');
    $state = $task['form_state']->getUserInput();
    $groupTypes = [];
    $var_options = '';
    $groupRef = '';
    foreach ($gtypes as $key => $val) {
      $groupTypes[$key] = $val['label'];

    }

    $form = [
      '#markup' => $this->t('True / False task for Groups or Group Roles.'),
    ];
    $options = '';
    if (isset($ifParms['groups']) && ($ifParms['groups'] != '') && ($ifParms['method'] != 'byvariable')) {

      $groupRef = Group::load($ifParms['groups']);

      /** @var \Drupal\group\Entity\GroupTypeInterface $gtype */
      $gtype = $groupRef->getGroupType();
      /** @var \Drupal\group\Entity\GroupRoleInterface[] $groupRoles */
      $groupRoles = $gtype->getRoles();
      $options = [];
      /** @var \Drupal\group\Entity\GroupRoleInterface $role */
      foreach ($groupRoles as $role) {
        $options[$role->label()] = $role->label();
      }
    }

    $groupRoles = [];
    if (count($task['form_state']->getValues()) > 0) {

      $groupRefVar = GroupType::load($task['form_state']->getValues()['byvariable']['select_assigned_group_by_var_type']);

      if ($groupRefVar) {
        $groupRoles = $groupRefVar->getRoles();

      }
    }
    else {
      $groupRefVar = GroupType::load($task['data']['GroupIf']['vargrouptype']);

      if ($groupRefVar) {

        $groupRoles = $groupRefVar->getRoles();
      }

    }

    $var_options = [];
    /** @var \Drupal\group\Entity\GroupRole $role */
    foreach ($groupRoles as $role) {

      if (!$role->get('internal')) {

        $var_options[$role->label()] = $role->label();

      }
    }

    $variables = MaestroEngine::getTemplateVariables($templateMachineName);
    $vars = [];
    foreach ($variables as $var) {
      $vars[$var['variable_id']] = $var['variable_id'];
    }

    $form['method'] = [

      '#type' => 'radios',
      '#title' => $this->t('Execute the IF:'),
      '#options' => [
        'byvariable' => $this->t('By Variable'),
        'byassignment' => $this->t('By Selecting A Group / Role'),
      ],
      '#default_value' => isset($ifParms['method']) ? $ifParms['method'] : 'byvariable',
      '#required' => TRUE,
      '#attributes' => [
        // 'onclick' => 'document.getElementById("byvar").setAttribute("open", "open");'.
        'onclick' => 'maestro_group_security_group_if_toggle(this);',
      ],
      '#attached' => [
        'library' => ['maestro_group_security/maestro_group_security_css'],
      ],
    ];

    /* ###### BY VARIABLE    */
    $form['byvariable'] = [
      '#id' => 'byvar',
      '#tree' => TRUE,
      '#type' => 'details',
      '#title' => $this->t('By Variable Options'),
      '#open' => FALSE,
    ];

    $form['byvariable']['select_assigned_group_by_var'] = [
      '#type' => 'select',
      '#default_value' => isset($ifParms['varname']) ? $ifParms['varname'] : '',
      '#title' => $this
        ->t('Select Variable'),
      '#options' => $vars,
    ];

    $form['byvariable']['select_assigned_group_by_var_type'] = [
      '#type' => 'select',
      '#default_value' => isset($ifParms['vargrouptype']) ? $ifParms['vargrouptype'] : '',
      '#title' => $this
        ->t('Select Group Type'),
      '#options' => $groupTypes,
      '#ajax' => [
        'callback' => 'Drupal\maestro_group_security\Controller\DefaultController::buildGroupTypeSelectIf',
        'event' => 'change',
        'wrapper' => 'variable_group_type_roles',
        'progress' => [
          'type' => 'throbber',
          'message' => t('loading roles...'),
        ],
      ],
    ];

    $form['byvariable']['select_assigned_group_by_var_roles'] = [

      '#attributes' => [
        'id' => 'select_assigned_group_role',
      ],
      '#type' => 'checkboxes',
      '#default_value' => isset($ifParms['roles']) ? $ifParms['roles'] : '',
      '#options' => $var_options,
      '#description' => 'limit access to only specific roles inside this group',
      '#title' => t('Group Roles'),
      '#required' => FALSE,
      '#prefix' => '<div  id="variable_group_type_roles">',
      '#suffix' => '</div>',
    ];

    /*
    'Budget Officer'=>[
    '#disabled' => true,
    '#attributes'=>[
    'checked' => "true"
    ]
    ]
     */

    if (isset($state['byvariable']['select_assigned_group_by_var_roles'])) {
      $vroles = $task['form_state']->getUserInput()['byvariable']['select_assigned_group_by_var_roles'];
    }
    else {
      $vroles = $task['data']['GroupIf']['vroles'];
    }

    foreach ($vroles as $vrole) {
      if ($vrole != "") {
        $form['byvariable']['select_assigned_group_by_var_roles'][$vrole] = [

          '#attributes' => [
            'checked' => "true",
          ],
        ];
      }
    }

    /* ############################ */

    /* ##### BY ASSIGN<ENT */

    $form['byassignment'] = [
      '#id' => 'byass',
      '#tree' => TRUE,
      '#type' => 'details',
      '#title' => $this->t('Select a Group / Role'),
      '#open' => FALSE,
    ];

    $form['byassignment']['select_assigned_group'] = [
      '#id' => 'select_assigned_group',
      '#type' => 'entity_autocomplete',
      '#target_type' => 'group',
      '#default_value' => isset($groupRef) ? $groupRef : '',
      '#selection_settings' => ['include_anonymous' => FALSE],
      '#title' => t('Group'),
      '#required' => FALSE,
      '#prefix' => '<div  >',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => 'Drupal\maestro_group_security\Controller\DefaultController::fetchRolesMembershipIfTask',
        'event' => 'autocompleteclose',
        'wrapper' => 'group_role_wrapper2',
        'progress' => [
          'type' => 'throbber',
          'message' => t('loading roles...'),
        ],
      ],
    ];

    $form['byassignment']['select_assigned_group_roles'] = [
      '#id' => 'select_assigned_group_role',
      '#attributes' => [
        'id' => ['select_assigned_group_role'],
      ],
      '#type' => 'checkboxes',
      '#default_value' => isset($ifParms['roles']) ? $ifParms['roles'] : '',
      '#options' => $var_options,
      '#description' => 'limit access to only specific roles inside this group',
      '#title' => t('Group Roles'),
      '#required' => FALSE,
      '#prefix' => '<div  ><div  id="group_role_wrapper2">',
      '#suffix' => '</div></div>',

    ];

    if (isset($state['byassignment']['select_assigned_group_roles'])) {
      $aroles = $state['byassignment']['select_assigned_group_roles'];
    }
    else {
      $aroles = $task['data']['GroupIf']['aroles'];
    }

    foreach ($aroles as $arole) {
      if ($arole != "") {
        $form['byassignment']['select_assigned_group_roles'][$arole] = [

          '#attributes' => [
            'checked' => "true",
          ],
        ];
      }
    }

    $task['data']['GroupIf']['aroles'] = $aroles;
    $task['data']['GroupIf']['vroles'] = $vroles;
    switch ($state['method']) {
      case "byvariable":
        $task['data']['GroupIf']['method'] = 'byvariable';
        $task['data']['GroupIf']['roles'] = $vroles;
        break;

      case "byassignment":
        $task['data']['GroupIf']['roles'] = $aroles;
        $task['data']['GroupIf']['method'] = 'byassignment';
        break;

    }

    $form['byassignment']['select_assigned_group']['#attributes']['onchange'] = "

    console.log('group change fired');

    function showGroup() {

      if (jQuery('#group_role_wrapper2').parent().length > 0) {
      //  jQuery('[class^=maestro-engine-assignments-hidden-group').show();
        jQuery('#group_role_wrapper2').parent().show();
          console.log('HIT');

      } else {
        console.log('MISS');
        setTimeout(showGroup,1000);

      }


    }

    setTimeout(showGroup,800);


    ";
    if (isset($ifParms['method']) && $ifParms['method'] == 'byvariable') {

      $form['byvariable']['#open'] = TRUE;
      $form['byassignment']['#open'] = FALSE;
    }
    else {
      $form['byvariable']['#open'] = FALSE;
      $form['byassignment']['#open'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritDoc}.
   */
  public function validateTaskEditForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritDoc}.
   */
  public function prepareTaskForSave(array &$form, FormStateInterface $form_state, array &$task) {
    // variable, operator, variable_value, status.
    $vals = $form_state->getUserInput();
    $groups = $vals['byassignment']['select_assigned_group'];

    $method = $vals['method'];
    $varname = $vals['byvariable']['select_assigned_group_by_var'];
    $vargrouptype = $vals['byvariable']['select_assigned_group_by_var_type'];

    if ($method == 'byvariable') {
      $roles = $vals['byvariable']['select_assigned_group_by_var_roles'];
    }
    else {
      $roles = $vals['byassignment']['select_assigned_group_roles'];
    }

    $groups = explode('(', $groups)[1];
    $groups = rtrim($groups, ')');

    $task['data']['GroupIf'] = [

      'groups' => $groups,
      'vroles' => $vals['byvariable']['select_assigned_group_by_var_roles'],
      'aroles' => $vals['byassignment']['select_assigned_group_roles'],
      'method' => $method,
      'varname' => $varname,
      'vargrouptype' => $vargrouptype,

    ];

  }

  /**
   * {@inheritDoc}.
   */
  public function performValidityCheck(array &$validation_failure_tasks, array &$validation_information_tasks, array $task) {

  }

  /**
   * {@inheritDoc}.
   */
  public function getTemplateBuilderCapabilities() {
    return ['edit', 'drawlineto', 'drawfalselineto', 'removelines', 'remove'];
  }

}
