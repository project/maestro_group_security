/*

jQuery.fn.showGroupForm = function(data) {
    jQuery('[class^=maestro-engine-assignments-hidden-group]').show();
};

console.log("Library Loaded");
*/
$ = jQuery;
$(document).ready(()=>{
  jQuery('[class^=maestro-engine-assignments-hidden]').hide();
  jQuery('.maestro-engine-assignments-hidden-user').show();
  jQuery('.group-fixed').hide();
  jQuery('.group-role').hide();
  jQuery('.group-variable').hide();
  jQuery("[id*=edit-edit-task-assignments-select-assigned-group]").val('');
  jQuery("[id*=select_assigned_group_role]").attr('checked',false);
  jQuery("[id*=edit-edit-task-assignments-select-assigned-group-type]").val('');
  jQuery(".maestro-engine-assignments-hidden-notification-user").show();


});


Drupal.behaviors.form_submit_processor = {
  attach: function (context, settings) {
   if (context.id == 'edit-task-form') {
     jQuery('[class^=maestro-engine-assignments-hidden]').hide();
     jQuery('.maestro-engine-assignments-hidden-user').show();
     jQuery("[id*=edit-edit-task-assignments-select-assigned-group]").val('');
     jQuery("[id*=select_assigned_group_role]").attr('checked', false);
     jQuery("[id*=edit-edit-task-assignments-select-assigned-group-type]").val('');
     jQuery(".maestro-engine-assignments-hidden-notification-user").show();
   }
  }
};





function maestro_group_security_interactive_task_fetch_roles(obj) {
  console.log("Fetch Roles Fired");
  switch(obj) {

    case "fixed":
      jQuery('.maestro-engine-assignments-group-fixed').show();
      jQuery('.maestro-engine-assignments-group-variable').hide();
      //  jQuery("[id^=edit-edit-task-assignments-select-assign-to]").append('<option value="user">User</option>');
      // jQuery("[id^=edit-edit-task-assignments-select-assign-to]").append('<option value="role">Role</option>');
      //  jQuery("[id^=edit-edit-task-assignments-select-assign-to]").append('<option value="group">Group</option>');

      break;

    case "variable":
      jQuery('.maestro-engine-assignments-group-fixed').hide();
      jQuery('.maestro-engine-assignments-group-variable').show();
      break;




  }

}



function maestro_group_security_group_toggle(obj) {


  if(obj.value !== undefined) {
    if(obj.value === 'variable') {
      jQuery('#byvar').show();
      jQuery('#byass').hide();
    }
    else {
      jQuery('#byass').show();
      jQuery('#byvar').hide();
    }
  }




}

function maestro_group_security_group_if_toggle(obj) {

console.log("Running...");
  if(obj.value !== undefined) {
    if(obj.value === 'byvariable') {
      jQuery('#byvar').attr('open', 'open');
      jQuery('#byass').removeAttr('open');
    }
    else {
      jQuery('#byass').attr('open', 'open');
      jQuery('#byvar').removeAttr('open');
    }
  }




}


function maestro_group_security_group_interactive_toggle(obj) {
  console.log("TOGGLING", obj);
 // jQuery("[id^=edit-edit-task-assignments-select-assign-to] option").remove();


  if(obj !== undefined) {
    switch(obj) {

      case "fixed":
        jQuery('.showfixed').show();
        jQuery('.showvariable').hide();
      //  jQuery("[id^=edit-edit-task-assignments-select-assign-to]").append('<option value="user">User</option>');
       // jQuery("[id^=edit-edit-task-assignments-select-assign-to]").append('<option value="role">Role</option>');
      //  jQuery("[id^=edit-edit-task-assignments-select-assign-to]").append('<option value="group">Group</option>');

        break;

      case "variable":
        jQuery('.showfixed').hide();
        jQuery('.showvariable').show();
        break;




    }




  }

}


function maestro_group_security_group_assign_to_change(obj) {



  jQuery('[class^=maestro-engine-assignments-hidden]').hide();

  switch (jQuery("[id^=edit-edit-task-assignments-select-method]").val()) {
    case "fixed":
      console.log("FIXED HIT");
      jQuery('.maestro-engine-assignments-hidden-variable').hide();
      if (obj == 'user') {
        jQuery('.maestro-engine-assignments-hidden-user').show();
      }
      if (obj == 'role') {
        jQuery('.maestro-engine-assignments-hidden-role').show();
      }
      break;
    case "variable":
      console.log("VARIABLE HIT");
      jQuery('.maestro-engine-assignments-hidden-variable').show();
      jQuery('.maestro-engine-assignments-hidden-role').hide();
      jQuery('.maestro-engine-assignments-hidden-user').hide();
      break;

  }


  if (obj == 'group') {

    switch (jQuery("[id^=edit-edit-task-assignments-select-method]").val()) {
      case "fixed":
        jQuery('.group-variable').hide();
        jQuery('.group-role').show();
        jQuery('.group-fixed').show();

        break;

      case "variable":
        jQuery('.group-fixed').hide();
        jQuery('.group-role').show();
        jQuery('.group-variable').show();

        break;
    }



  } else {


   // jQuery('[class^=maestro-engine-assignments-hidden-' + obj + ']').show();

  }








  }




  function maestro_group_security_task_editor_assignments_assignby(obj) {
    jQuery('[class^=maestro-engine-assignments-hidden-notification]').hide();

    let assto = '';
  switch (obj) {
    case "fixed":
      jQuery('.maestro-engine-assignments-hidden-variable').hide();


       assto = jQuery('[id^=edit-edit-task-assignments-select-assign-to]').val();
      console.log("ASSIGNED TO: ",assto);
      if (assto == 'group') {
        jQuery('.group-variable').hide();
        jQuery('.group-role').show();
        jQuery('.group-fixed').show();
      }
      if (assto == 'user') {
        jQuery('.maestro-engine-assignments-hidden-user').show();
      }
      if (assto == 'role') {
        jQuery('.maestro-engine-assignments-hidden-role').show();
      }
      break;
    case "variable":
      jQuery('.maestro-engine-assignments-hidden-variable').show();
      jQuery('.maestro-engine-assignments-hidden-role').hide();
      jQuery('.maestro-engine-assignments-hidden-user').hide();
       assto = jQuery('[id^=edit-edit-task-assignments-select-assign-to]').val();
      console.log("ASSIGNED TO: ",assto);
      if (assto == 'group') {
        jQuery('.group-fixed').hide();
        jQuery('.group-role').show();
        jQuery('.group-variable').show();
      }
      break;

  }

  }




  //###### NOTIFICATION FORM


function maestro_group_security_task_editor_notification_assignby(obj) {
  jQuery("[class^=maestro-engine-assignments-hidden-notification]").hide();

  let assto = "";
  switch (obj) {
    case "fixed":
      jQuery(".maestro-engine-assignments-hidden-notification-variable").hide();

      assto = jQuery("[id^=edit-edit-task-notifications-select-notification-to]").val();
      console.log("ASSIGNED TO: ", assto);
      if (assto == "group") {
        jQuery(".notification-group-variable").hide();
        jQuery(".notification-group-role").show();
        jQuery(".notification-group-fixed").show();
      }
      if (assto == "user") {
        jQuery(".maestro-engine-assignments-hidden-notification-user").show();
      }
      if (assto == "role") {
        jQuery(".maestro-engine-assignments-hidden-notification-role").show();
      }
      break;
    case "variable":
      jQuery(".maestro-engine-assignments-hidden-notification-variable").show();
      jQuery(".maestro-engine-assignments-hidden-notification-role").hide();
      jQuery(".maestro-engine-assignments-hidden-notification-user").hide();
      assto = jQuery("[id^=edit-edit-task-notifications-select-notification-to]").val();
      console.log("NOTIFY TO: ", assto);
      if (assto == "group") {
        jQuery(".notification-group-fixed").hide();
        jQuery(".notification-group-role").show();
        jQuery(".notification-group-variable").show();
      }
      break;
  }
}


function maestro_group_security_group_which_notification_change(val) {

  if (val) {
    console.log("NOTi TYPE CHANGE!", val);
  jQuery(".form-item-edit-task-notifications-notification-assignment").parent().hide();
  jQuery(".form-item-edit-task-notifications-notification-escalation").parent().hide();
  jQuery(".form-item-edit-task-notifications-notification-reminder").parent().hide();

  switch (val) {
    case 'assignment':
      jQuery(".form-item-edit-task-notifications-notification-assignment").show();
      jQuery(".form-item-edit-task-notifications-notification-assignment").parent().show();
    break;
    case 'escalation':
      jQuery(".form-item-edit-task-notifications-notification-escalation").show();
      jQuery(".form-item-edit-task-notifications-notification-escalation").parent().show();
    break;
    case 'reminder':
      jQuery(".form-item-edit-task-notifications-notification-reminder").show();
      jQuery(".form-item-edit-task-notifications-notification-reminder").parent().show();
    break;
  }
}
}


function maestro_group_security_group_notify_to_change(obj) {
  jQuery("[class^=maestro-engine-assignments-hidden-notification]").hide();

  switch (jQuery("[id^=edit-edit-task-notifications-select-notification-method]").val()) {
    case "fixed":
      console.log("FIXED HIT");
      jQuery(".maestro-engine-assignments-hidden-notification-variable").hide();
      if (obj == "user") {
        jQuery(".maestro-engine-assignments-hidden-notification-user").show();
      }
      if (obj == "role") {
        jQuery(".maestro-engine-assignments-hidden-notification-role").show();
      }
      break;
    case "variable":
      console.log("VARIABLE HIT");
      jQuery(".maestro-engine-assignments-hidden-notification-variable").show();
      jQuery(".maestro-engine-assignments-hidden-notification-role").hide();
      jQuery(".maestro-engine-assignments-hidden-notification-user").hide();
      break;
  }

  if (obj == "group") {
    switch (jQuery("[id^=edit-edit-task-notifications-select-notification]").val()) {
      case "fixed":
        jQuery(".notification-group-variable").hide();
        jQuery(".notification-group-role").show();
        jQuery(".notification-group-fixed").show();

        break;

      case "variable":
        jQuery(".notification-group-fixed").hide();
        jQuery(".notification-group-role").show();
        jQuery(".notification-group-variable").show();

        break;
    }
  } else {
    // jQuery('[class^=maestro-engine-assignments-hidden-' + obj + ']').show();
  }
}






